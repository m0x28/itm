/****************************************************************************************************************//**
 * @file    main.c
 * @author  Maxim Ivanchenko
 * @date    July 2018
 * @brief   Test of inter-thread messaging module.
 * Two threads send a string to the main thread.
 *
 * Building: $ gcc itm-test.c itm.c ../liteq/liteq.c -I../ -DITM_TEST -lpthread
 * Testing:  $ valgrind --leak-check=yes ./a.out 1000
 *******************************************************************************************************************/
#ifdef ITM_TEST
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <assert.h>
#include <unistd.h> // sleep
#include <errno.h> 
#include <limits.h>  // LONG_MAX, LONG_MIN
#include "itm.h"
/*******************************************************************************************************************/
#define DEFAULT_ITER_NUM 	100
/*******************************************************************************************************************/
tItm gItm;
tItm gItm1;
tItm gItm2;
int gHalfIterNum = DEFAULT_ITER_NUM / 2;
/*******************************************************************************************************************/
void* Thread1(void* pNull)
{
	int i = gHalfIterNum;
	usleep(100000);
	ItmCreate(&gItm1);
	"Thread1 id=%lu\n", pthread_self();

	while(i--)
	{
		char str1[] = "string1";
		int resInt = ItmSend(&gItm, 55, str1, sizeof(str1), NULL);
		if(resInt)
		{
			// Data freeing here, if it was dynamically allocated
		}
		usleep(1000);
	}
	pthread_exit(NULL);
}
/*******************************************************************************************************************/
void* Thread2(void* pNull)
{
	int i = gHalfIterNum;
	usleep(100000);
	ItmCreate(&gItm2);
	printf("Thread2 id=%lu\n", pthread_self());

	while(i--)
	{
		char str2[] = "string2";
		int resInt = ItmSend(&gItm, 66, str2, sizeof(str2), NULL);
		if(resInt)
		{
			// Data freeing here, if it was dynamically allocated
		}
		usleep(1500);
	}
	pthread_exit(NULL);
}
/*******************************************************************************************************************/
int main(int argc, char* argv[])
{
	pthread_t id1, id2;

    int iterNum = DEFAULT_ITER_NUM;
    if(argc > 1)
    {
        errno = 0;
        long int arg = strtol(argv[1], NULL, 10);
        if ((errno == ERANGE && (arg == LONG_MAX || arg == LONG_MIN)) || (errno != 0 && arg == 0))
        {
            perror("liteq-test: strtol");
            return EXIT_FAILURE;
        }
        iterNum = (int)arg;
    }
    gHalfIterNum = iterNum / 2;

	ItmCreate(&gItm);
	pthread_create(&id1, NULL, Thread1, NULL);
	pthread_create(&id2, NULL, Thread2, NULL);

 	printf("The test of \"itm\".\n"
    "You may enter a number of iteration as the argument of the current executable.\n");
 
	while(iterNum--)
	{
		tItmMsg* pMsg = NULL;
		if(!ItmRecv(&pMsg, &gItm))
		{
			if(pMsg)
			{
				printf("data=%s type=%d size=%d src=%lu\n", (char*)pMsg->pData, pMsg->type, pMsg->size, pMsg->src);
				ItmFreeMsg(pMsg);
			}
			else
			{
				printf("msg is NULL!\n");
			}
		}		
		else
		{
			perror("liteq-test: ItmRecv");
            return EXIT_FAILURE;
		}
	}
	ItmDestroy(&gItm2);
	ItmDestroy(&gItm1);
	ItmDestroy(&gItm);
	pthread_join(id1, NULL); // To avoid memleak
	pthread_join(id2, NULL);
	exit(EXIT_SUCCESS); 
}
#endif // ITM_TEST
/*******************************************************************************************************************/
