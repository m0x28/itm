/****************************************************************************************************************//**
 * @file    itm.h
 * @author  Maxim Ivanchenko
 * @date    July 2018
 * @brief   Inter-thread messaging for Linux
 * 
 * Assumptionos and conjectures:
 * 1. Sanity deceision not to check the return values of all Posix APIs: If the itm Initiation was successful, then 
 *    any error during Sending and Receiving could happen only by user's cause: wrong object treaing, for inatance.
 * 	  Don't proceed with Posix functions, when first face an error - because will they likely failed too.
 * 
 * 2. The proper messaging stopping is the user's responsibility. It must provide not to sending messages to entity, 
 *    which called the Destroying function.
 *******************************************************************************************************************/
#ifndef __INTERTHREAD_MESSAGING_H__
#define __INTERTHREAD_MESSAGING_H__
/*******************************************************************************************************************/
#include <pthread.h>
#include <semaphore.h>
#include <stdint.h>
#include <errno.h>
#include "liteq/liteq.h"
/*******************************************************************************************************************/
#ifdef __cplusplus
extern "C" {
#endif
/*******************************************************************************************************************/
#define ITM_ERR__BAD_ARG	(EINVAL)
#define ITM_ERR__NOMEM 	    (ENOMEM)
#define ITM_ERR__SEM_INIT	(-3)
#define ITM_ERR__SEM_WAIT	(-4)
#define ITM_ERR__SEM_POST	(-5)
#define ITM_ERR__SEM_DESTR	(-6)
#define ITM_ERR__MTX_INIT	(-7)
#define ITM_ERR__MTX_LOCK	(-8)
#define ITM_ERR__MTX_UNLOCK (-9)
#define ITM_ERR__MTX_DESTR  (-10)
#define ITM_ERR__FIFO_INIT	(-11)
#define ITM_ERR__FIFO_PUSH	(-12)
#define ITM_ERR__FIFO_POP	(-13)
/*******************************************************************************************************************/
/// Inter-thread messaging entity
typedef struct
{
	pthread_t id; ///< Owner thread id
	pthread_mutex_t mutex; ///< Queue protection
	sem_t msgs; ///< Number of incoming messages
	tLiteq fifo; ///< Queue of incoming messages
}
tItm;
/*******************************************************************************************************************/
/// Inter-thread message
typedef struct 
{
	int32_t   type;
	pthread_t src;
	pthread_t dst;
	void* 	  pData;
	int32_t   size;
	pthread_mutex_t* pMutex; ///< If null - buffer was dynamically allocated and needed freeing, otherwise - pointer to a mutex which protects this buffer
}
tItmMsg;
/****************************************************************************************************************//**
 * @brief  Create itm entity. Pointer can be as static so dynamic.
 * @return 0 - ok. Other - error.
 *******************************************************************************************************************/
int ItmCreate(tItm* pThis/** [in] itm entity*/);
/****************************************************************************************************************//**
 * @brief  Destroy itm entity
 * @return 0 - ok. Other - error.
 *******************************************************************************************************************/
int ItmDestroy(tItm* pThis /** [in] itm entity*/);
/****************************************************************************************************************//**
 * @brief  Receive a message from itm entity
 * @warning Cleanup: use ItmFreeMsg() or
 * 			<*ppMsg> MUST be freed after the message is handled!
 *          <pData> of the message also MUST be freed if the <pMutex> of the message is NULL!
 * @return 0 - ok. Negative - error.
 *******************************************************************************************************************/
int ItmRecv
(
	tItmMsg** 	ppMsg, 	///< [out] Received message
	tItm* 		pThis	///< [in]  The itm entity, which receives the message
);
/****************************************************************************************************************//**
 * @brief  Clean up a message and it's data
 *******************************************************************************************************************/
void ItmFreeMsg
(
	tItmMsg* 	pMsg 	///< [in] The received message to be cleaned
);
/****************************************************************************************************************//**
 * @brief  Send a message to itm entity.
 * @return 0 - ok. Negative - error (-errno is used to denote an error).
 *******************************************************************************************************************/
int ItmSend
(
	tItm* 	pDst,		///< [in]  Destination entity
	int32_t msgType,	///< [in]  User protocol message type
	void* 	pData,      ///< [in]  Data. If <pMutex> is NULL, data will be copied to dynamically allocated buffer
	int 	size,		///< [in]  Data size in number of bytes
	pthread_mutex_t* pMutex ///<  [in] Set this to NULL to dynamically allocate buffer or provide a pointer to mutex, if data is protected by it
);
/*******************************************************************************************************************/
// This type might be used as a keeper. Create something like <tItmMan tasks[NUM_OF_THREADS_IN_YOUR_APP]>
typedef struct sItmManager
{
	char name[16];   	///< Thread name
	char shortName[4];	///< Thread short name
	tItm itm;			///< Intertask messaging entity
}
tItmMan;
/*******************************************************************************************************************/
#ifdef __cplusplus
}
#endif
/*******************************************************************************************************************/
#endif // __INTERTHREAD_MESSAGING_H__
