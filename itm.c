/****************************************************************************************************************//**
 * @file    itm.c
 * @author  Maxim Ivanchenko
 * @date    July 2018
 * @brief   Inter-thread messaging for Linux
 *******************************************************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <memory.h>
#include <assert.h>
#include <fcntl.h>    /* For O_* constants */
#include <sys/stat.h> /* For mode constants */

#include <sys/stat.h>
#include <sys/types.h>

#include "liteq/liteq.h"
#include "itm.h"
/****************************************************************************************************************//**
 * @brief  Construct a semaphore name. (Used for named semaphore only)
 *******************************************************************************************************************/
void ConstructSemName(char(*pStr)[24])
{
	int size = sizeof(*pStr);
	memset(pStr, 0, size);
	memcpy(pStr, "/sema_", 7);
	sprintf(&((char*)pStr)[6], "%x", (unsigned int)pthread_self()); // If a 64-bit system, take only lower 32-bit value
}
/****************************************************************************************************************//**
 * @brief  Create itm entity. Pointer can be as static so dynamic.
 * @return 0 - ok. Other - error.
 *******************************************************************************************************************/
int ItmCreate(tItm* pThis/** [in] itm entity*/)
{
	if(!pThis)
	{
		return ITM_ERR__BAD_ARG;
	}

	memset(pThis, 0, sizeof(tItm));

	pThis->id = pthread_self();

	if(LiteqInit(&pThis->fifo)) return ITM_ERR__FIFO_INIT;
	if(sem_init(&pThis->msgs, 0, 0)) return ITM_ERR__SEM_INIT;
	if(pthread_mutex_init(&pThis->mutex, NULL)) {sem_destroy(&pThis->msgs); return ITM_ERR__MTX_INIT;}

	return EXIT_SUCCESS;
}
/****************************************************************************************************************//**
 * @brief  Destroy itm entity
 * Before destroying entity, you must notify other threads to stop sending sending messages to the destroying entity!
 * @return 0 - ok. Other - the first error occurred.
 *******************************************************************************************************************/
int ItmDestroy(tItm* pThis /** [in] itm entity*/)
{
	int e = EXIT_SUCCESS;

	// Flush fifo
	if(pthread_mutex_lock(&pThis->mutex)) return ITM_ERR__MTX_LOCK;
	int msgNum = LiteqNum(&pThis->fifo);
	while(msgNum > 0)
	{
		void* p = NULL;
		msgNum = LiteqPop((void**)&p, &pThis->fifo);
		printf("!!!! LiteqPop in ItmDestroy ret=%d\n", msgNum);
		if(msgNum >= 0)
		{
			printf(" free in Destroy\n");
			free(p);
		}
	}
	if(pthread_mutex_unlock(&pThis->mutex)) return ITM_ERR__MTX_UNLOCK;

	if(pthread_mutex_destroy(&pThis->mutex)) return ITM_ERR__MTX_DESTR;
	if(sem_destroy(&pThis->msgs)) return ITM_ERR__SEM_DESTR;

	return e;
}
/****************************************************************************************************************//**
 * @brief  Receive a message from itm entity
 * @warning Cleanup: use ItmFreeMsg() or
 * 			<*ppMsg> MUST be freed after the message is handled!
 *          <pData> of the message also MUST be freed if the <pMutex> of the message is NULL!
 * @return 0 - ok. Other - the first error occurred.
 *******************************************************************************************************************/
int ItmRecv
(
	tItmMsg** 	ppMsg, 	///< [out] Received message
	tItm* 		pThis	///< [in]  The itm entity, which receives the message
)
{
	if(sem_wait(&pThis->msgs)) return ITM_ERR__SEM_WAIT;
	if(pthread_mutex_lock(&pThis->mutex)) return ITM_ERR__MTX_LOCK;
	if(LiteqPop((void**)ppMsg, &pThis->fifo) < 0) return ITM_ERR__FIFO_POP;
	if(pthread_mutex_unlock(&pThis->mutex)) return ITM_ERR__MTX_UNLOCK;
	return EXIT_SUCCESS;
}
/****************************************************************************************************************//**
 * @brief  Clean up a message and it's data
 *******************************************************************************************************************/
void ItmFreeMsg
(
	tItmMsg* 	pMsg 	///< [in] The received message to be cleaned
)
{
	/* Don't check for pMsg != NULL, because ItmFreeMsg() is called after ItmRecv() and already known by it's return
	value, whether the message was received successfully or not. */

	if(!pMsg->pMutex)
	{
		free(pMsg->pData);
	}
	free(pMsg);
}
/****************************************************************************************************************//**
 * @brief  Send a message to itm entity.
 * @return 0 - ok. Other - the first error occurred.
 *******************************************************************************************************************/
int ItmSend
(
	tItm* 	pDst,		///< [in]  Destination entity
	int32_t msgType,	///< [in]  User protocol message type
	void* 	pData,      ///< [in]  Data. If <pMutex> is NULL, data will be copied to dynamically allocated buffer
	int 	size,		///< [in]  Data size in number of bytes
	pthread_mutex_t* pMutex ///<  [in] Set this to NULL to dynamically allocate buffer or provide a pointer to mutex, if data is protected by it
)
{
	int e = EXIT_SUCCESS;
	pthread_mutex_t* pDstMutex;
	tItmMsg* pMsg = malloc(sizeof(tItmMsg));
	if(!pMsg)
	{
		return ITM_ERR__NOMEM;
	}

	pMsg->type = msgType;
	pMsg->src = pthread_self();
	pMsg->pData = pData;
	pMsg->size = size;
	pMsg->dst = pDst->id;
	pMsg->pMutex = pMutex;

	if(!pMutex)
	{
		void* p = malloc(size);
		if(!p)
		{
			free(pMsg);
			return ITM_ERR__NOMEM;
		}
		memcpy(p, pData, size);
		pMsg->pData = p;
	}

	pDstMutex = &pDst->mutex;

	do
	{
		if(pthread_mutex_lock(pDstMutex)) {e = ITM_ERR__MTX_LOCK; break;}
		if(LiteqPush(pMsg, &pDst->fifo) <= 0) {e = ITM_ERR__FIFO_PUSH; break;}
		if(pthread_mutex_unlock(pDstMutex)) {e = ITM_ERR__MTX_UNLOCK; break;}
		if(sem_post(&pDst->msgs)) {e = ITM_ERR__SEM_POST; break;}
	}
	while(0);
	
	if(e)
	{
		if(!pMutex)
		{
			free(pMsg->pData);
		}
		free(pMsg);

		if(e == ITM_ERR__SEM_POST) // No sense to do it at the rest errors
		{
			pthread_mutex_lock(pDstMutex);
			LiteqPop(NULL, &pDst->fifo); 
			pthread_mutex_unlock(pDstMutex);
		}
	}
	return e;
}
/*******************************************************************************************************************/
